+++
author = "Kay Kim"
title = "OpenGL Lightings"
date = "2021-01-04"
description = "OpenGL 4.3"
tags = []
+++

<img src="/images/opengl_01.png" width="640px"/>
<br>

Illumination model with Gouraud shading, Phong shading, Blinn-Phong shading.

***

<img src="/images/opengl_02.png" width="640px"/>
<br>

Implementation of Reflection, Refraction, Sky Box and Cube Mapping.

all of these were written in C++, with OpenGL 4.3.