---
title: "Old Game Projects"
date: 2020-08-10T08:43:05+09:00
draft: false
---

**La tour (2016)**

{{< youtube yIu5hseuZo0 >}}







Top-view 2D action game with custom C++ engine  
Project of DigiPen GAM250 class

***

**High noon (2016)**

![alt text](/images/game_2.png "game_2")





Side-scrolling 2D action game in a one month game jam
Created by Zero Editor and its Zilch script

***

**The Moon (2017)**

![alt text](/images/game_3.png "game_3")





Jump-it-up game for testing functionalities of custom C++ engine


***

**The Informer (2017)**

![](/images/game_4.png "game_4")





Another simple game for the same purpose