+++
author = "Kay Kim"
title = "Auto-Battler Terrain Generation"
date = "2020-08-29"
description = "Peek"
tags = []
+++

Hello there!

I'd love to play auto-battler genre from 2019 - DOTA AUTO CHESS was the beginning, and followed by Teamfight Tactics and Battlegrounds of Hearthstone. All the factors from the genre were fascinating for me so I'm making my own one with Unity 2019. Here are some quick peeks of the project!



***

<img class="Hex01-thumbnail" src="/images/Hex_01.png" width="640px"/>
<br>

Brief look of my hexagonal terrain with offset coordinate system.

***

<img class="Hex02-thumbnail" src="/images/Hex_02.png" width="640px"/>
<br>

Elevation is presented as terraces and cliffs. Gaps between cells are filled with blending of cell textures.


***

<img class="Hex03-thumbnail" src="/images/Hex_03.png" width="640px"/>
<br>

Here are some terrain features - waterfall, water, walls, buildings, trees, grass, etc.

***

![alt text](/images/Hex_04.gif "Hex_04")
<br>

Pathfinding is essential feature for auto-battler genre - Adapt A* with priority queue.






***

References :


[Red Blob Games](https://www.redblobgames.com/grids/hexagons/) for hexagonal coordinate system


[Jasper Flick](https://twitter.com/catlikecoding) for mesh manipulation in Unity