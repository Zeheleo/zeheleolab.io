---
title: "Introduction"
date: 2020-07-28T19:19:34+09:00
draft: false
---

### About

Hello, I'm Kay Kim.  

I'm a junior at DigiPen Institute of Technology majoring in Computer Science in Real-Time Interactive Simulation.  

Love to make something interesting, playing piano (totally novice) and surfing.  

---

### Contact

you can contact me through [LinkedIn](https://www.linkedin.com/in/) or [G-mail](mailto:sync774@gmail.com).